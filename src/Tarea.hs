module Tarea where


-- Dada una lista de numeros, devuelve la suma de los mismos. {-AKA: sum -}
sumatoria :: [Int] -> Int
sumatoria = undefined


-- Dada una lista de elementos, devuelve la cantidad. {-AKA: lenght -}
longitud :: [a] -> Int
longitud = undefined


-- Dado un elemento y una lista, indica la cantidad de veces que esta en la lista
apariciones :: Eq a => a -> [a] -> Int
apariciones = undefined


-- Dada una lista, determina si todos los elementos son iguales
todosIguales :: Eq a => [a] -> Bool
todosIguales []       = True
todosIguales [x]      = True
todosIguales (x:y:xs) = x == y && todosIguales (y:xs)


-- Dada una lista de booleanos, indicar si todos ellos son True
todoVerdad :: [Bool] -> Bool
todoVerdad = undefined


-- Dada una lista de booleanos, indica si alguno de ellos es True
algunaVerdad :: [Bool] -> Bool
algunaVerdad = undefined


-- Dado un elemento y una lista, indica si el elemento está en la lista. {-AKA: elem -}
pertenece :: Eq a => a -> [a] -> Bool
pertenece = undefined


-- Dado una lista, elige el mayor de los elementos. {-AKA: maximum -}
-- Parcial si la lista esta vacia
masGrande :: Ord a => [a] -> a
masGrande = undefined


