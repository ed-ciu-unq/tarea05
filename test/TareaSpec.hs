module TareaSpec (spec) where
import Test.Hspec
import Tarea

spec :: Spec
spec =
  describe "Pruebas para la tarea" $ do

    it "sumatoria" $
      sumatoria [2,8,5] `shouldBe` 15

    it "longitud" $
      longitud "Villanueva" `shouldBe` 10

    it "apariciones" $
      apariciones 'I' "MISSISSIPPI" `shouldBe` 4

    it "todosIguales False" $
      todosIguales "AHHHH" `shouldBe` False

    it "todosIguales True" $
      todosIguales  "55555555" `shouldBe` True

    it "todoVerdad 1" $
      todoVerdad [] `shouldBe` True

    it "todoVerdad 2" $
      todoVerdad [True,True] `shouldBe` True

    it "todoVerdad 3" $
      todoVerdad [True,True,False] `shouldBe` False

    it "algunaVerdad 1" $
      algunaVerdad [] `shouldBe` False

    it "algunaVerdad 2" $
      algunaVerdad [True,True] `shouldBe` True

    it "algunaVerdad 3" $
      algunaVerdad [True,True,False] `shouldBe` True

    it "pertenece 1" $
      pertenece 2 [3,5,24] `shouldBe` False

    it "pertenece 2" $
      pertenece 5 [3,5,24] `shouldBe` True

    it "pertenece 3" $
      pertenece 2 [] `shouldBe` False

    it "masGrande" $
      masGrande [5,3,8,4] `shouldBe` 8


