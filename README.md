# FRANKENSTEIN: Estructuras + Haskell + git

El proyecto Frankenstein consiste en una cantidad de tareas, en orden creciente de dificultad, que cubren el contenido de la parte de Haskell de Estructuras de Datos.

El objetivo principal en Frankenstein consiste en “matar los _undefined_”. Un _undefined_ representa un espacio en blanco que se debe completar con código que cumpla la funcionalidad esperada.

El comando `stack test` permite correr los tests de la tarea. Al principio todos ellos deben estar en rojo. La meta es que todos pasen a estar en verde.

## Tarea 05: Folds sobre listas

En las funciones que usan el patrón FOLD (plegar, como haciendo un origami), la lista es reducida a un solo valor, ya sea un máximo, mínino o alguna otra operación. Muchas veces no es siquiera necesario recorrer toda la lista.

Este patrón es tan poderoso y genérico que map y filter son casos particulares de fold.

---

Autor: Román García (nykros@gmail.com)
